package de.tr808axm.spigot.ratethatplot;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.ProxiedCommandExecutor;
import de.tr808axm.spigot.ratethatplot.command.RateThatPlotCommand;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This plugin's main class.
 * Created by tr808axm on 09.07.2016.
 */
public class Main extends JavaPlugin {
    private final ChatUtil chatUtil = new ChatUtil(getDescription(), ChatColor.BLUE, getServer());
    private SQLiteDatabaseConnector databaseConnector;
    private List<UUID> unratedPlayers;

    @Override
    public void onLoad() {
        super.onLoad();
        if(!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        databaseConnector = new SQLiteDatabaseConnector(getLogger(), new File(getDataFolder(), "ratings.sqlite").getPath());
        try {
            databaseConnector.prepareStatement("CREATE TABLE IF NOT EXISTS ratings (UUID TEXT,atmosphereScore INTEGER,styleScore INTEGER,detailsScore INTEGER,purposeScore INTEGER, note TEXT)").executeUpdate();
        } catch (SQLException e) {
            getLogger().severe("Error at creating table 'ratings' in database: " + e.getClass().getSimpleName() + ": " + e.getMessage());
            getLogger().info("Disabling...");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        getLogger().info("Loaded!");
    }

    @Override
    public void onEnable() {
        super.onEnable();
        registerCommands();
        getLogger().info("Enabled!");
    }

    @Override
    public void onDisable() {
        super.onDisable();
        databaseConnector.closeConnection();
        getLogger().info("Disabled!");
    }

    private void registerCommands() {
        getCommand("ratethatplot").setExecutor(new RateThatPlotCommand(this));
        getCommand("rate").setExecutor(new ProxiedCommandExecutor(getCommand("ratethatplot"), "rate"));
        getCommand("rating").setExecutor(new ProxiedCommandExecutor(getCommand("ratethatplot"), "rating"));
    }

    public ChatUtil getChatUtil() {
        return chatUtil;
    }

    public OfflinePlayer getOfflinePlayerIfExists(String name) {
        @SuppressWarnings("deprecation") OfflinePlayer offlinePlayer = getServer().getOfflinePlayer(name);
        return offlinePlayer.getLastPlayed() == 0 ? null : offlinePlayer;
    }

    public Rating saveRating(UUID uuid, Rating rating) {
        if(uuid == null) throw new IllegalArgumentException("uuid may not be null");
        if(rating == null) throw new IllegalArgumentException("rating may not be null");

        Rating oldRating = getRating(uuid);
        if(oldRating != null) {
            try {
                PreparedStatement statement = databaseConnector.prepareStatement("UPDATE ratings SET atmosphereScore=?,styleScore=?,detailsScore=?,purposeScore=?,note=?");
                statement.setShort(1, rating.getAtmosphereScore());
                statement.setShort(2, rating.getStyleScore());
                statement.setShort(3, rating.getDetailsScore());
                statement.setShort(4, rating.getPurposeScore());
                statement.setString(5, rating.getNote());
                statement.executeUpdate();
            } catch (SQLException e) {
                getLogger().severe("An exception occurred while updating rating of " + uuid.toString() + ": " + e.getClass().getSimpleName() + ": " + e.getMessage());
            }
        } else {
            try {
                PreparedStatement statement = databaseConnector.prepareStatement("INSERT INTO ratings (UUID,atmosphereScore,styleScore,detailsScore,purposeScore,note) VALUES(?,?,?,?,?,?)");
                statement.setString(1, uuid.toString());
                statement.setShort(2, rating.getAtmosphereScore());
                statement.setShort(3, rating.getStyleScore());
                statement.setShort(4, rating.getDetailsScore());
                statement.setShort(5, rating.getPurposeScore());
                statement.setString(6, rating.getNote());
                statement.executeUpdate();
            } catch (SQLException e) {
                getLogger().severe("An exception occurred while inserting rating of " + uuid.toString() + ": " + e.getClass().getSimpleName() + ": " + e.getMessage());
            }
        }
        return oldRating;
    }

    public Rating getRating(UUID uuid) {
        if(uuid == null) throw new IllegalArgumentException("uuid may not be null");

        try {
            PreparedStatement statement = databaseConnector.prepareStatement("SELECT * FROM ratings WHERE UUID = ?");
            statement.setString(1, uuid.toString());

            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                return new Rating(resultSet.getShort("atmosphereScore"),
                        resultSet.getShort("styleScore"),
                        resultSet.getShort("detailsScore"),
                        resultSet.getShort("purposeScore"),
                        resultSet.getString("note"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * This method may be very expensive. Use with caution.
     */
    public boolean searchUnratedPlayers() {
        List<UUID> ratedPlayers = new ArrayList<>();
        try {
            PreparedStatement statement = databaseConnector.prepareStatement("SELECT * FROM ratings");

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ratedPlayers.add(UUID.fromString(resultSet.getString("UUID")));
            }
        } catch (SQLException e) {
            getLogger().severe("Exception at reading UUIDs from database: " + e.getClass().getSimpleName() + ": " + e.getMessage());
            return false;
        }
        unratedPlayers = new ArrayList<>();
        for(OfflinePlayer offlinePlayer : getServer().getOfflinePlayers()) {
            if(!ratedPlayers.contains(offlinePlayer.getUniqueId())) unratedPlayers.add(offlinePlayer.getUniqueId());
        }
        return true;
    }

    public List<UUID> getUnratedPlayers() {
        if(unratedPlayers == null) searchUnratedPlayers();
        return unratedPlayers;
    }
}
