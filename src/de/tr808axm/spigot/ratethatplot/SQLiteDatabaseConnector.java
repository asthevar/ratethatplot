package de.tr808axm.spigot.ratethatplot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Used to connect to a Database.
 * Created by tr808axm on 11.07.2016.
 */
public class SQLiteDatabaseConnector {
    private final String filepath;
    private final Logger logger;

    private Connection connection;

    public SQLiteDatabaseConnector(Logger logger, String filepath) {
        this.filepath = filepath;
        this.logger = logger;
        connect();
    }

    private void connect() {
        logger.info("SQLiteDatabaseConnector: Connecting to Database...");
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + filepath);
        } catch (SQLException e) {
            logger.severe("SQLiteDatabaseConnector: Error at connecting to database: " + e.getClass().getSimpleName() + ": " + e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.severe("SQLiteDatabaseConnector: Driver not found: " + e.getClass().getSimpleName() + ": " + e.getMessage());
        }
    }

    public void closeConnection() {
        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.severe("SQLiteDatabaseConnector: Error at closing da");
                e.printStackTrace();
            }
        }
    }

    public PreparedStatement prepareStatement(String query) throws SQLException {
        return connection.prepareStatement(query);
    }
}
